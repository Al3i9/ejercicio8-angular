import { Component, OnInit } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Component({
  selector: 'app-contador',
  templateUrl: './contador.component.html',
  styleUrls: ['./contador.component.css']
})
export class ContadorComponent implements OnInit {

  mostrar : number = 0;
  advertencia : boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  sumar(){
    if(this.mostrar === 50){
      this.advertencia = true;
    }else{
      this.mostrar = this.mostrar + 1 ;
      this.advertencia = false;
    }
  }

  restar(){
    if(this.mostrar === 0){
      this.advertencia = true;
    }else{
      this.mostrar = this.mostrar - 1 ;
      this.advertencia = false;
    }
  }

}
